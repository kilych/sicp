### Test

#### Run all tests

`$ make test`

#### Run single test

`$ cd <path/to/project>`

`$ guile test/<test_file>`
