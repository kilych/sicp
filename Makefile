.PHONY: test count-lines count-present-lines count-sloc

test:
	for file in test/*test.scm; do guile $$file; done

count-lines:
	find src -regextype posix-extended -regex ".*\.scm" | xargs wc -l

# empty lines excluded
count-present-lines:
	find src -regextype posix-extended -regex ".*\.scm" | xargs grep '^.'    | cut -d':' -f1 | uniq -c | awk '{ sum += $$1; print } END { print sum, " total" }'

# empty lines and comments excluded
count-sloc:
	find src -regextype posix-extended -regex ".*\.scm" | xargs grep '^[^;]' | cut -d':' -f1 | uniq -c | awk '{ sum += $$1; print } END { print sum, " total" }'
